*** DEPENDENCIES ***
————————————————————
VUE 3, VUEX
————————————————————
sudo npm uninstall --global vue-cli
sudo npm uninstall --global @vue/cli
sudo npm install --global @vue/cli@latest
vue --version || vue -V
create Vue App -> vue create <app-name>
cd app-name
npm install bootstrap axios
npm run serve

VS CODE -> Vue VS Code Extension Pack -> Extension  

--------------------
RESOURCES
--------------------
https://www.youtube.com/watch?v=vBMY7251-Jg￼ ... Naveen Saggam

--------------------
VUEX LIFECYCLE
--------------------
Vue components dispatch actions
Actions commit mutations  … and … Actions deal with the backend API
Mutations mutate the state … and … Mutations are shown in the dev-tools
States are then rendered in Vue components


