export default {
    namespaced: true,
    state: {
        counter: {
            count: 0
        }
    },
    mutations: {
        INC_COUNTER: function (state) {
            state.counter.count = state.counter.count + 1;
        },
        INC_COUNTER_BY: function (state, payload) {
            state.counter.count = state.counter.count + payload.value;
        },
        DECR_COUNTER: function (state) {
            state.counter.count = state.counter.count - 1;
        }
    },
    actions: {
        incrementCounter: function ({commit}) {
            return commit("INC_COUNTER");
        },
        incrementCounterBy: function ({commit}, payload) {
            return commit("INC_COUNTER_BY", payload);
        },
        decrementCounter: function ({commit}) {
            return commit("DECR_COUNTER");
        }
    }
}