import axios from "axios";

export default {
    state: {
        usersList: {
            loading: false,
            users: [],
            errorMessage: null
        }
    },
    mutations: {
        GET_USERS_PENDING: function (state) {
            state.usersList.loading = true; 
        },
        GET_USERS_FULFILLED: function (state,payload) {
            state.usersList.loading = false;
            state.usersList.users = payload.users;
        },
        GET_USERS_REJECTED: function (state, payload) {
            state.usersList.loading = false;
            state.usersList.errorMessage = payload.error;
        }
    },
    actions: {
        getUsers: async function ({commit}) {
            try {
                commit("GET_USERS_REQUEST");
                let baseURL = `https://jsonplaceholder.typicode.com/users`;
                let response = await axios.get(baseURL);
                commit("GET_USERS_FULFILLED", {users: response.data});
            }
            catch (error) {
                commit("GET_USERS_REJECTED", {error: error});
            }
        }
    }
}