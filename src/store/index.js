import { createStore } from 'vuex';
import counterModule from './modules/counterModule';
import employeesModule from './modules/employeesModule';
import usersModule from './modules/usersModule';

export default createStore({
  state: {
    counterState: counterModule.state,
    employeesState: employeesModule.state,
    usersState: usersModule.state
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    counterModule,
    employeesModule,
    usersModule
  },
  getters: {
    getCounterState: function (state) {
      return state.counterState.counter;
    },
    getEmployeeState: function (state) {
      return state.employeesState.employeesList;
    },
    getUsersState: function (state) {
      return state.usersState.usersList;
    }
  }
})
